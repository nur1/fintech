﻿using Fintech.Entity;
using System.Data.Entity;

namespace Fintech.Facade
{
    public class FintechDBContext : DbContext
    {
        public FintechDBContext():base("FINTECHDB") { }
        public DbSet<AccountBanks> AccountBanks { get; set; }    
        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<Banks> Banks { get; set; }
        public DbSet<Balances> Balances { get; set; }
        public DbSet<Merchants> Merchants { get; set; }
        public DbSet<TopUps> TopUps { get; set; }
        public DbSet<Transactions> Transactions { get; set; }
    }
}

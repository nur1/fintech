﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Fintech.Entity;

namespace Fintech.Facade
{
    public class AccountFacade : BaseFacade<Accounts>
    {
        public override async Task<bool> Insert(Accounts entity)
        {
            try
            {
                entity.LastModifiedDate = DateTime.Now;
                entity.RegisterDate = DateTime.Now;
                string passw = Context.Database.SqlQuery<string>("SELECT [dbo].SaltPassword({0})", "P@ssw0rd").FirstOrDefault();
                entity.Password = passw;
                Context.Accounts.Add(entity);
                int affctd = await Context.SaveChangesAsync();
                return affctd > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetSaltPassword(string password)
        {
            try
            {
                var sql = "SELECT [dbo].[SaltPassword](@0)";
                var param = new SqlParameter("@0", password);
                var passw = Context.Database.SqlQuery<string>(sql, param).SingleOrDefault();
                return passw;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Insert(Accounts account, List<AccountBanks> banks)
        {
            using(var txn = Context.Database.BeginTransaction())
            {
                try
                {
                    Context.Accounts.Add(account);
                    int affctd = await Context.SaveChangesAsync();
                    foreach (AccountBanks ab in banks)
                    {
                        ab.AccountID = account.AccountID;
                    }
                    //add account banks
                    Context.AccountBanks.AddRange(banks);
                    //add new balance for new account inserted
                    var balance = new Balances();
                    balance.AccountID = account.AccountID;
                    balance.Balance = 50000;
                    balance.RetainedBalance = 0;

                    Context.Balances.Add(balance);
                    affctd = await Context.SaveChangesAsync();
                    txn.Commit();
                    return affctd > 0;
                }
                catch (Exception ex)
                {
                    txn.Rollback();
                    throw ex;
                }
            }
        }

        public async Task<bool> Update(Accounts account, List<AccountBanks> banks)
        {
            using(var tsc = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0,1,0)))
            {
                try
                {
                    Context.Entry<Accounts>(account).State = EntityState.Modified;
                    var listbank = Context.AccountBanks.Where(a => a.AccountID == account.AccountID);
                    Context.AccountBanks.RemoveRange(listbank);
                    Context.AccountBanks.AddRange(banks);
                    int affctd = await Context.SaveChangesAsync();
                    tsc.Complete();
                    return affctd > 0;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            
        }

        public virtual async Task<bool> Delete(int id)
        {
            try
            {
                var account = GetByID(id);
                var balance = Context.Balances.SingleOrDefault(b => b.AccountID == id);
                if (account == null)
                {
                    throw new Exception("Data not found!");
                }
                var sql = "SELECT COUNT(1) FROM Transactions WHERE AccountID = @0";
                var param = new SqlParameter("@0", id);
                var txnCount = Context.Database.SqlQuery<int>(sql, param).SingleOrDefault();
                sql = "SELECT COUNT(1) FROM Topups WHERE AccountID = @0";
                param = new SqlParameter("@0", id);
                var topupCount = Context.Database.SqlQuery<int>(sql, param).SingleOrDefault();


                if (txnCount > 0 || topupCount > 0)
                {
                    //you can throw this exception if required
                    //throw new Exception("This account can not be deleted because already used in transaction");
                    account.Status = false;
                    balance.RetainedBalance += balance.Balance;
                    balance.Balance = 0;
                }
                else
                {
                    var listAccountBanks = Context.AccountBanks.Where(a => a.AccountID == id);
                    Context.AccountBanks.RemoveRange(listAccountBanks);
                    Context.Balances.Remove(balance);
                    Context.Accounts.Remove(account);
                }

                int affctd = await Context.SaveChangesAsync();
                return affctd > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

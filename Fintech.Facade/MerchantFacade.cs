﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fintech.Entity;

namespace Fintech.Facade
{
    public class MerchantFacade : BaseFacade<Merchants>
    {
        public Merchants GetMerchantByAccountID(int accountID)
        {
            try
            {
                return Context.Merchants.FirstOrDefault(m => m.AccountID == accountID);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}

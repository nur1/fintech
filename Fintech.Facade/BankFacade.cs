﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fintech.Entity;

namespace Fintech.Facade
{
    public class BankFacade : BaseFacade<Banks>
    {
        public Banks GetByAccountID(int id)
        {
            try
            {
                return Context.Banks.SingleOrDefault(b => b.AccountID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

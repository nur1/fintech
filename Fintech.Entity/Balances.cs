﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fintech.Entity
{
    [Table("Balances")]
    public class Balances
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountID { get; set; }

        [Required]
        public decimal Balance { get; set; }

        [Required] 
        public decimal RetainedBalance { get; set; }
         
        [Required]
        public DateTime LastTransaction { get; set; } 
    }
}

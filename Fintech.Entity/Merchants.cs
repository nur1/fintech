﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fintech.Entity
{
    [Table("Merchants")]
    public class Merchants
    {
        [Key]
        public int MerchantID { get; set; }

        [Required, MaxLength(50)]
        public string MerchantName { get; set; }

        [Required, MaxLength(128)]
        public string Description { get; set; }

        [Required, MaxLength(50)]
        public string ContactName { get; set; }

        [Required, MaxLength(60)]
        public string Address { get; set; }

        [MaxLength(30)]
        public string City { get; set; }

        [MaxLength(10)]
        public string PostalCode { get; set; }

        [Required, MaxLength(40)]
        public string Email { get; set; }

        [Required, MaxLength(25)]
        public string Phone { get; set; }

        [Required]
        public int AccountID { get; set; }
    }
}

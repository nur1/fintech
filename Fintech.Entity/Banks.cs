﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fintech.Entity
{
    [Table("Banks")]
    public class Banks
    {
        [Key]
        public int BankID { get; set; }

        [Required, MaxLength(50)]
        public string BankName { get; set; }

        [MaxLength(50)]
        public string HeadOffice { get; set; }

        [MaxLength(50)]
        public string ContactName { get; set; }

        [MaxLength(25)]
        public string ContactPhone { get; set; }

        [MaxLength(50)]
        public string ContactEmail { get; set; }

        [Required]
        public int AccountID { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fintech.Entity
{
    [Table("TopUps")]
    public class TopUps
    {
        [Key]
        public int TopupID { get; set; }

        [Required, JsonConverter(typeof(DateTimeConverter))]
        public DateTime TopupDate { get; set; }

        [Required]
        public int AccountID { get; set; }


        public int BankID { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required, MaxLength(50)]
        public string ReferenceNumber { get; set; }

        [Required, MaxLength(50)]
        public string TxnNumber { get; set; }

        [Required, MaxLength(9)]
        public string Status { get; set; }

        [Required, JsonConverter(typeof(DateTimeConverter))]
        public DateTime CreatedDate { get; set; }

        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime? ProcessDate { get; set; } 
    }

    public class TopUpsView
    {
        public int TopupID { get; set; }

        public DateTime TopupDate { get; set; }

        public int AccountID { get; set; }

        public int BankID { get; set; }
        public string  BankName { get; set; }
        public decimal Amount { get; set; }

        public string ReferenceNumber { get; set; }

        public string TxnNumber { get; set; }

        public string Status { get; set; }

        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime CreatedDate { get; set; }

        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime? ProcessDate { get; set; }
    }
}

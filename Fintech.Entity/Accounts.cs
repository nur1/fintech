﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fintech.Entity
{
    [Table("Accounts")]
    public class Accounts
    {
        [Key]
        public int AccountID { get; set; }

        [Required, MaxLength(8)]
        public string AccountType { get; set; }

        [Required, MaxLength(50)]
        public string FirstName { get; set; }

        [Required, MaxLength(50)]
        public string LastName { get; set; }

        [Required, MaxLength(30)]
        public string PlaceOfBirth { get; set; }

        [Required, JsonConverter(typeof(DateTimeConverter))]
        public DateTime DateOfBirth { get; set; }

        [Required, MaxLength(50)]
        public string MotherName { get; set; }

        [Required, MaxLength(50)]
        public string IDNumber { get; set; }

        [Required, MaxLength(60)]
        public string Address { get; set; }
         
        [MaxLength(30)]
        public string City { get; set; }
         
        [MaxLength(40)]
        public string State { get; set; }
         
        [MaxLength(10)]
        public string PostalCode { get; set; }
         
        [MaxLength(30)]
        public string Country { get; set; }

        [Required, MaxLength(50)]
        public string Email { get; set; }

        [Required, MaxLength(25)]
        public string Phone { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public bool Status { get; set; }

        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime RegisterDate { get; set; }

        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime LastModifiedDate { get; set; }       
    }

    public class DateTimeConverter : IsoDateTimeConverter
    {
        public DateTimeConverter()
        {
            DateTimeFormat = "yyyy-MMM-dd";
        }
    }
}

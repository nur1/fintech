﻿using Fintech.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fintech.WebMVC.Models
{
    public class AccountModel
    {
        public Accounts Account { get; set; }

        public List<Banks> ListBank { get; set; }

        public Balances Balance { get; set; }

        public List<TopUps> ListTopup { get; set; }

    }
}
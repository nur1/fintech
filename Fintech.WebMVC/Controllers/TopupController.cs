﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CodeID.Helper;
using Fintech.Common;
using Fintech.Entity;
using Fintech.Facade;
using Fintech.WebMVC.Filter;
using Newtonsoft.Json;

namespace Fintech.WebMVC.Controllers
{
    [@Authorize("Customer")]
    public class TopupController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult Bank()
        {
            return View();
        }


        [HttpPost]
        public async Task<ActionResult> Insert(TopUps topup)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new TopupFacade())
                    {
                        var status = await facade.Insert(topup);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(topup));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(topup));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Update(TopUps topup)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new TopupFacade())
                    {
                        var status = await facade.Update(topup);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(topup));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(topup));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Paging(PageRequest page)
        {
            if (ModelState.IsValid)
            {
                page.Criteria.Add(new Criteria { criteria = "AccountID", value = Commons.GetUserID() });
                try
                {
                    using (var facade = new TopupFacade())
                    {
                        var pgResult = await facade.GetPage(page);
                        var result = JsonConvert.SerializeObject(ApiResponse.OK(pgResult));
                        return Content(result, "application/json");
                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(page));
            }
        }

        //PagingBank
        [HttpPost]
        public async Task<ActionResult> PagingBank(PageRequest page)
        {
            if (ModelState.IsValid)
            {
                var bankid = "-1";
                try
                {
                    using (var facade = new TopupFacade())
                    {
                        var id = Convert.ToInt32(Commons.GetUserID());
                        var account = new AccountFacade().GetByID(id);
                        if (account.AccountType.Equals("Bank"))
                        {
                            var bank = new BankFacade().GetByAccountID(id);
                            bankid = bank.BankID + "";
                        }
                        page.Criteria.Add(new Criteria { criteria = "BankID", value = bankid });
                        var pgResult = await facade.GetPage(page);
                        var result = JsonConvert.SerializeObject(ApiResponse.OK(pgResult));
                        return Content(result, "application/json");
                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(page));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new TopupFacade())
                    {
                        var status = await facade.Delete(id);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(id));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(id));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Accepted(int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new TopupFacade())
                    {
                        var status = await facade.Accepted(id);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(id));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(id));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Rejected(int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new TopupFacade())
                    {
                        var status = await facade.Rejected(id);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(id));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(id));
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CodeID.Helper;
using Fintech.Common;
using Fintech.Entity;
using Fintech.Facade;
using Fintech.WebMVC.Filter;
using Fintech.WebMVC.Models;
using Newtonsoft.Json;

namespace Fintech.WebMVC.Controllers
{
    //[@Authorize]
    public class BankController : Controller
    {
        // GET: Account
        public ActionResult Index()
        { 
            return View();
        }

        public ActionResult IntroAngularJs()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Insert(Banks bank)
        { 
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new BankFacade())
                    {
                        var status = await facade.Insert(bank);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(bank));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(bank));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Update(Banks bank)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new BankFacade())
                    {
                        var status = await facade.Update(bank);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(bank));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(bank));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Paging(PageRequest page)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new BankFacade())
                    {
                        var pgResult = await facade.GetPage(page);
                        var result = JsonConvert.SerializeObject(ApiResponse.OK(pgResult));
                        return Content(result, "application/json");
                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(page));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new BankFacade())
                    {
                        var status = await facade.Delete(id);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(id));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(id));
            }
        }


        public async Task<ActionResult> IntroRazor()
        {
            using(var facade = new BankFacade())
            {
                var list = (await facade.GetAll()).ToList();
                ViewBag.ListData = list.ToList();
                //ViewData["ListData"] = list;
                //Session["ListData"] = list;
                //var model = new AccountModel();
                //model.Account = new Accounts();
                //model.ListBank = new List<Banks>();
                //model.ListTopup = new List<TopUps>();
                //model.Balance = new Balances();
                return View(list);
            }
        }

        public ActionResult Detail(int id)
        {
            using (var facade = new BankFacade())
            {
                var bank = facade.GetByID(id);  
                return View(bank);
            }
        }

        public ActionResult DetailAjax(int id)
        {
            using (var facade = new BankFacade())
            {
                var bank = facade.GetByID(id);
                return PartialView(bank);
            }
        }
    }
}
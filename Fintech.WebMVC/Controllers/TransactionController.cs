﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CodeID.Helper;
using Fintech.Common;
using Fintech.Entity;
using Fintech.Facade;
using Fintech.WebMVC.Filter;
using Newtonsoft.Json;

namespace Fintech.WebMVC.Controllers
{
    [@Authorize]
    public class TransactionController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            using(var facade = new MerchantFacade())
            {
                var id = Convert.ToInt32(Commons.GetUserID());
                var merchant = facade.GetMerchantByAccountID(id);
                if(merchant is null)
                {
                    ViewBag.MerchantID = 0;
                }
                else
                {
                    ViewBag.MerchantID = merchant.MerchantID;
                }

            }
            return View();
        }

        public ActionResult Customer()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Insert(Transactions transaction)
        { 
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new TransactionFacade())
                    {
                        var status = await facade.Insert(transaction);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(transaction));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(transaction));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Update(Transactions transaction)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new TransactionFacade())
                    {
                        var status = await facade.Update(transaction);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(transaction));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(transaction));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Paging(PageRequest page)
        {
            if (ModelState.IsValid)
            {                                
                try
                {
                    using (var facade = new TransactionFacade())
                    {
                        var id = Convert.ToInt32(Commons.GetUserID());
                        var accnt = new AccountFacade().GetByID(id);
                        var mercntid = "-1";
                        if (accnt.AccountType.Equals("Merchant"))
                        {
                            var mercnt = new MerchantFacade().GetMerchantByAccountID(accnt.AccountID);
                            mercntid = mercnt.MerchantID.ToString();
                        } 
                        page.Criteria.Add(new Criteria { criteria = "MerchantID", value = mercntid });
                        var pgResult = await facade.GetPage(page);
                        var result = JsonConvert.SerializeObject(ApiResponse.OK(pgResult));
                        return Content(result, "application/json");
                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(page));
            }
        }

        public async Task<ActionResult> PagingCustomer(PageRequest page)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    page.Criteria.Add(new Criteria {criteria="AccountID", value=Commons.GetUserID() });
                    using (var facade = new TransactionFacade())
                    {
                        var pgResult = await facade.GetPage(page);
                        var result = JsonConvert.SerializeObject(ApiResponse.OK(pgResult));
                        return Content(result, "application/json");
                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(page));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new TransactionFacade())
                    {
                        var status = await facade.Delete(id);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(id));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(id));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Accepted(int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new TransactionFacade())
                    {
                        var status = await facade.Accepted(id);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(id));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(id));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Rejected(int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new TransactionFacade())
                    {
                        var status = await facade.Rejected(id);
                        if (status)
                        {
                            return Json(ApiResponse.OK(status));
                        }
                        else
                        {
                            return Json(ApiResponse.NotAcceptable(id));
                        }

                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest(id));
            }
        }

    }
}